import { Component } from '@angular/core';
import { Medicion } from './medicion';
import { Storage } from '@ionic/storage';
import { Papa } from 'ngx-papaparse';
import { Platform } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  table: Medicion[] = [];
  headerRow: string[] = ['index', 'numSerie', 'horaInicio', 'horaFin', 'pesoNeto', 'tiempoMinutos', 'resultadoFinal']
  // Modelo de row ->
  // {
  //   numSerie: 0,
  //   horaInicio: hora:mins,
  //   horaFin: hora:mins,
  //   pesoNeto: 0,
  // }


  constructor(
    private storage: Storage, 
    private papa: Papa,
    private plt: Platform,
    private file: File,
    private socialSharing: SocialSharing
    )
  { 
    this.storage.get('mediciones').then((val) => {
      if (val == null ) {
        this.setupTables()
        let newMed = new Medicion();
        newMed.index = 0
        this.table = [...this.table, newMed];
      } else {
      this.table = val.sort((a, b) => { return a.index - b.index})
      .filter(e => {
            let horasFinal = e.horaFin.getTime() / 1000 / 60 / 60
            let horasHoy = new Date().getTime() / 1000 / 60 / 60
            return (horasHoy - horasFinal) <= 24
          })

          let newMed = new Medicion();
          if (this.table.length > 0) {
            newMed.index = this.table[this.table.length - 1].index + 1;
          } else {
            newMed.index = 0
          }
          this.table = [...this.table, newMed];
        
        }
    });
  }

  addRowToTable() {
    console.log('adding row');
    let newMed = new Medicion();
    if (this.table.length > 0) {
      newMed.index = this.table[this.table.length - 1].index + 1;
    } else {
      newMed.index = 1
    }
    this.table = [...this.table, newMed];
  }

  addZero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }

  getIndexByIndex(index) {
    for (let i = 0; i < this.table.length; i++) {
      
      if (index == this.table[i].index) {
        return i;
      }
    }
    return -1;
  }

  handleNumSerie(index) {

    if (index == this.table[this.table.length - 1].index || this.table.length == 0) {
      this.addRowToTable();
    }
    
    if (this.table[this.getIndexByIndex(index)].numSerie.length == 6 && this.table[this.getIndexByIndex(index)].horaInicio == undefined) {
      this.table[this.getIndexByIndex(index)].horaInicio = new Date();
    }

    
  }

  handlePesoNeto(index) {
    if (this.table[this.getIndexByIndex(index)].horaFin == undefined && this.table[this.getIndexByIndex(index)].horaInicio != undefined) {
      this.table[this.getIndexByIndex(index)].horaFin = new Date();
    }

    this.table[this.getIndexByIndex(index)].tiempoMinutos = this.calculateMins(index)
    this.table[this.getIndexByIndex(index)].resultadoFinal = this.calculateRatio(index)

    this.storeRow(this.table[this.getIndexByIndex(index)])


  }

  storeRow(medicion) {
    let newStorageTable = []
    if (medicion.numSerie != "") {
      this.storage.get('mediciones').then((val) => {

        let tempVal = val.filter(e => { return e.index == medicion.index})
        if (tempVal.length > 0) {
          val[tempVal.index] = medicion
          newStorageTable = val
        } else {
          medicion.index = val.length 
          newStorageTable = [...val,  medicion]
        }
        this.storage.set('mediciones', newStorageTable);


      })
    }
  }

  calculateMins(index) {
    const tiempoTranscurrido = this.table[this.getIndexByIndex(index)].horaFin.getTime() - this.table[this.getIndexByIndex(index)].horaInicio.getTime();
    return Math.round(tiempoTranscurrido / 1000 / 60) > 0 ? Math.round(tiempoTranscurrido / 1000 / 60) : 1 ;
  }

  calculateRatio(index) {
    return 60 * this.table[this.getIndexByIndex(index)].pesoNeto / this.calculateMins(index) / 1000;
  }

  setupTables() {
    console.log('setting up');
    this.table = [];
    this.storage.set('mediciones', []);
  }

  exportCSV() {
    this.storage.get('mediciones').then((val) => {
      let csv = this.papa.unparse({
        fields: this.headerRow,
        data: val
      });
      if (this.plt.is('cordova')) {
        this.file.writeFile(this.file.dataDirectory, 'data.csv', csv, {replace: true}).then( res => {
          this.socialSharing.share(null, null, res.nativeURL, null).then(e =>{
            // Success
          }).catch(e =>{
            console.log('Share failed:', e)
          });
        }, err => {
          console.log('Error: ', err);
        });
      } else {
        // Dummy implementation for Desktop download purpose
        var blob = new Blob([csv]);
        var a = window.document.createElement('a');
        a.href = window.URL.createObjectURL(blob);
        a.download = 'newdata.csv';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      }
  })
}
}
