export class Medicion {
    index: number = 0
    numSerie: string = ""
    horaInicio: Date
    horaFin: Date
    pesoNeto: number
    tiempoMinutos: number
    resultadoFinal: number
}